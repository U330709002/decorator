package observer;

public class WeatherStation {

	double currentTemp=3;
	
	double currentHumidity=5;
	
	double currentPressure=6;

	public double getCurrentTemp() {
		return currentTemp;
	}

	public double getCurrentHumidity() {
		return currentHumidity;
	}

	public double getCurrentPressure() {
		return currentPressure;
	}
	
	
}
