package observer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame {

	JTextField txt;
	public MyFrame(String title){
		super(title);
		JButton btn = new JButton("Click me");
		this.getContentPane().add(btn, BorderLayout.NORTH);
		btn.addActionListener(new ButtonListener());
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				MyFrame.this.setTitle("Title Changed");
				
			}
		});
		
		
		txt = new JTextField();
		this.getContentPane().add(txt, BorderLayout.SOUTH);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable()
	    {
	      public void run()
	      {
	        // create a jframe, giving it an initial title
	    	  MyFrame frame = new MyFrame("Simple JFrame Demo");
	        
	        // set the jframe size. in a more complicated application you will
	        // probably call frame.pack() before displaying it.
	        frame.setSize(new Dimension(300,200));
	        
	        // center the frame
	        frame.setLocationRelativeTo(null);
	        
	        // set this so your application can easily be stopped/killed
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        // display the frame
	        frame.setVisible(true);
	      }
	    });
	}

	private class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			txt.setText("Button Clicked");
			
		}
		
	}
}
