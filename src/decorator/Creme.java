package decorator;

public class Creme extends CondimentDecorator {

	public Creme(Beverage decoratedBeverage) {
		super(decoratedBeverage);
	}
	
	@Override
	public float cost() {
		return (float) 0.3 + decoratedBeverage.cost();
	}

	@Override
	public String getDescription() {
		return decoratedBeverage.getDescription() + " " + "Creme";
	}

}
