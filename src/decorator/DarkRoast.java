package decorator;

public class DarkRoast extends Beverage {

	
	
	public DarkRoast() {
		description = "Dark Roast";
	}
	@Override
	public float cost() {
		return (float) 4.5;
	}

}
