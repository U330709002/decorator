package decorator;

public class Decaf extends Beverage {

	public Decaf() {
		description = "Decaf";
	}
	@Override
	public float cost() {
		return (float) 3.5;
	}

}
