package decorator;

public class Milk extends CondimentDecorator {

	public Milk(Beverage decoratedBeverage) {
		super(decoratedBeverage);
	}
	
	
	
	@Override
	public float cost() {
		return (float) 0.75 + decoratedBeverage.cost();
	}

	@Override
	public String getDescription() {
		return decoratedBeverage.getDescription() + " " + "Milk";
	}

}
